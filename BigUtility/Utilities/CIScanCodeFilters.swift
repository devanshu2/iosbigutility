//
//  CIScanCodeFilters.swift
//  BigUtility
//
//  Created by Devanshu Saini on 03/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//
import CoreGraphics
import UIKit

open class CIScanCodeFilters: NSObject {
    open var backgroundColor: UIColor = UIColor.white
    
    open var foregroundColor: UIColor = UIColor.black
    
    static let sharedInstance : CIScanCodeFilters = {
        let instance = CIScanCodeFilters()
        return instance
    }()
    
    open func getResizedImageWithCIImage(_ coreImage: CIImage, size: CGSize) -> UIImage?{
        let extent: CGRect = coreImage.extent.integral
        let scale: CGFloat = min(size.width / extent.width, size.height / extent.height)
        let width: Int = Int(extent.width * scale)
        let height: Int = Int(extent.height * scale)
        let colorSpace:CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapContext = CGContext(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context: CIContext = CIContext()
        let bitmapImage: CGImage = context.createCGImage(coreImage, from: extent)!
        bitmapContext!.interpolationQuality = CGInterpolationQuality.none
        bitmapContext?.scaleBy(x: scale, y: scale)
        bitmapContext?.draw(bitmapImage, in: extent)
        let scaledImage: CGImage = bitmapContext!.makeImage()!
        return UIImage(cgImage: scaledImage)
    }
    
    open func getWaterMarkedFilteredImage(_ codedImage: CIImage?, bottomText: String, containerSize: CGSize) -> UIImage? {
        if codedImage == nil {
            return nil
        }
//        let codedImage = self.makdeColorFilterOverImage(xcodedImage!)
        var imageX: CGFloat = 0.0
        let imageY: CGFloat = 0.0
        var imageH: CGFloat = codedImage!.extent.size.height
        var imageW: CGFloat = codedImage!.extent.size.width
        let textHeight: CGFloat = 20.0
        let filterImageArea = CGSize(width: containerSize.width, height: containerSize.height - textHeight)
        let newHeightIfMaxWidth = filterImageArea.width * (imageH / imageW)
        if newHeightIfMaxWidth > filterImageArea.height {
            imageW = filterImageArea.height * (imageW / imageH)
            assert(imageW <= filterImageArea.width, "Invalid image dimension for container size: \(containerSize) and image size: \(codedImage?.extent.size)")
            imageH = filterImageArea.height
            imageX = (filterImageArea.width - imageW)/2
        }
        else{
            imageH = newHeightIfMaxWidth
            imageW = filterImageArea.width
        }
        let textY: CGFloat = imageH
        let convertedImage: UIImage = UIImage(ciImage: codedImage!)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: containerSize.width, height: textY + textHeight), false, 0.0);
        convertedImage.draw(in: CGRect(x: imageX, y: imageY, width: imageW, height: imageH));
        Constants.Colors.FilterImageTextColor.set()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let textFontAttributes = [
            NSFontAttributeName: Constants.Fonts.CodeAdTextFont!,
            NSParagraphStyleAttributeName: paragraphStyle,
            NSForegroundColorAttributeName: self.foregroundColor,
        ]
        bottomText.draw(with: CGRect(x: 0, y: textY, width: containerSize.width, height: textHeight), options: .usesLineFragmentOrigin, attributes: textFontAttributes, context: nil)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    open func makdeColorFilterOverImage(_ codedImage:CIImage) -> CIImage? {
        let filterColor: CIFilter = CIFilter(name: "CIFalseColor")!
        filterColor.setValue(codedImage, forKey: "inputImage")
        filterColor.setValue(self.foregroundColor.coreImageColor, forKey: "inputColor0")
        filterColor.setValue(self.backgroundColor.coreImageColor, forKey: "inputColor1")
        return filterColor.outputImage
    }
    
    open func getCodeImage(_ text: String, codeType: String) -> CIImage? {
        var returnImage: CIImage?
        switch codeType {
        case Constants.CIScanCodeFiltersName.AztecCode:
            returnImage = self.getAztecCodeImage(text)
        case Constants.CIScanCodeFiltersName.Barcode128:
            returnImage = self.get128BarcodeImage(text)
        case Constants.CIScanCodeFiltersName.BarcodePDF417:
            returnImage = self.getPDF417BarcodeImage(text)
        case Constants.CIScanCodeFiltersName.QRCode:
            returnImage = self.getQRCodeImage(text)
        default:
            returnImage = nil
        }
        return returnImage
    }
    
    fileprivate func getAztecCodeImage(_ text: String) -> CIImage? {
        let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        if let filter = CIFilter(name: Constants.CIScanCodeFiltersName.AztecCode) {
            filter.setDefaults()
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue(NSNumber(value: 90.0 as Float), forKey: "inputCorrectionLevel")
//            filter.setValue(NSNumber(float: 0.0), forKey: "inputLayers")
            filter.setValue(NSNumber(value: 0.0 as Float), forKey: "inputCompactStyle")
            if let output = filter.outputImage {
                return output
            }
        }
        return nil
    }
    
    fileprivate func get128BarcodeImage(_ text: String) -> CIImage? {
        let data = text.data(using: String.Encoding.ascii, allowLossyConversion: false)
        if let filter = CIFilter(name: Constants.CIScanCodeFiltersName.Barcode128) {
            filter.setValue(data, forKey: "inputMessage")
            if let output = filter.outputImage {
                return output
            }
        }
        return nil
    }
    
    fileprivate func getPDF417BarcodeImage(_ string: String) -> CIImage? {
        let data = string.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: Constants.CIScanCodeFiltersName.BarcodePDF417) {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            if let output = filter.outputImage?.applying(transform) {
                return output
            }
        }
        return nil
    }
    
    fileprivate func getQRCodeImage(_ text: String) -> CIImage? {
        let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        if let filter = CIFilter(name: Constants.CIScanCodeFiltersName.QRCode) {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("Q", forKey: "inputCorrectionLevel")
            if let output = filter.outputImage {
                return output
            }
        }
        return nil
    }
}
