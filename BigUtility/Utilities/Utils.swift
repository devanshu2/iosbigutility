//
//  Utils.swift
//  BigUtility
//
//  Created by Devanshu Saini on 18/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit

public class Utils: NSObject {
    static public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        #if DEBUG
            Swift.print(items, separator:separator, terminator: terminator)
        #endif
    }
    
    static public func getDateFromVEventTimeStamp(theTimeStamp timeStamp:String) -> Date? {
        let fmts = ["yyyyLLdd'T'HHmmssZ", "yyyyLLdd'T'HHmmss", "yyyyLLdd", "yyyyLLdd'T'HHmmss'Z'"]
        var result: Date?
        for fmt in fmts {
            let formatter = DateFormatter()
            formatter.dateStyle = .full
            formatter.dateFormat = fmt;
            formatter.locale = NSLocale.current
            if let date = formatter.date(from: timeStamp) {
                result = date as Date
                break;
            }
        }
        return result
    }
    
    static public func isValidURLString(urlString theURLString:String) ->Bool {
        if let urlTial:URL = URL(string: theURLString) {
            if ((urlTial.scheme != nil) && (urlTial.host != nil)) {
                if UIApplication.shared.canOpenURL(urlTial) {
                    return true
                }
            }
        }
        return false
    }
    
    static public func getLocalName(forBarCodeType theBarCode:BarCodeDataType) ->String {
        switch theBarCode {
        case .URL:
            return NSLocalizedString("URL", comment: "URL")
        case .VCARD:
            return NSLocalizedString("VCard", comment: "VCard")
        case .SMS:
            return NSLocalizedString("SMS", comment: "SMS")
        case .Call:
            return NSLocalizedString("Phone Number", comment: "Phone Number")
        case .GeoLocation:
            return NSLocalizedString("Geo Location Coordinates", comment: "Geo Location Coordinates")
        case .Event:
            return NSLocalizedString("Event", comment: "Event")
        case .Email:
            return NSLocalizedString("Email", comment: "Email")
        case .Wifi:
            return NSLocalizedString("Wifi", comment: "Wifi")
        case .String:
            return NSLocalizedString("Text", comment: "Text")
        default:
            return NSLocalizedString("", comment: "")
        }
    }
}
