//: Playground - noun: a place where people can play

import Foundation
import UIKit
//import Contacts
import AddressBook

public enum BarCodeDataType: String {
    case URL = "URL"
    case VCARD = "VCARD"
    case SMS = "SMS"
    case Call = "CALL"
    case GeoLocation = "GeoLocation"
    case Event = "Event"
    case Email = "Email"
    case Wifi = "Wifi"
    case String = "String"
}

var nDate = Date(timeIntervalSince1970: 20160923);


var dataType: BarCodeDataType = .String

var capturedString:String = "BEGIN:VCARD\nVERSION:3.0\nN:Saini;Devanshu\nFN:Devanshu Saini\nORG:Macroapps Technologies Pvt Ltd\nTITLE:CEO\nADR:;;Bandhwari;Gurgaon;Haryana;122101;India\nTEL;WORK;VOICE:+91 70114 13881\nTEL;CELL:+918447523757\nTEL;FAX:\nEMAIL;WORK;INTERNET:devanshu@macroapps.com\nURL:http://www.macroapps.com\nBDAY:15-Jun-1989\nEND:VCARD\n"

capturedString
//capturedString = "SMSTO:+91234232342:some message\nhello there"
//capturedString.components(separatedBy: "MATMSG:TO:")[1]
//capturedString = "BEGIN:VEVENT\nSUMMARY:Some event title\nDTSTART:20160923T190000Z\nDTEND:20160928T190000Z\nEND:VEVENT"

public func getDateFromVEventTimeStamp(theTimeStamp timeStamp:String) -> Date? {
    let fmts = ["yyyyLLdd'T'HHmmssZ", "yyyyLLdd'T'HHmmss", "yyyyLLdd", "yyyyLLdd'T'HHmmss'Z'"]
    var result: Date?
    for fmt in fmts {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.dateFormat = fmt;
        formatter.locale = NSLocale.current
        if let date = formatter.date(from: timeStamp) {
            result = date as Date
            break;
        }
    }
    return result
}

let dtStr = "20160923T190000Z"
getDateFromVEventTimeStamp(theTimeStamp: dtStr)

let splitData:Array<String>? = capturedString.components(separatedBy: ":")
if splitData != nil {
    if (splitData?.count)! > 0 {
        let identifierString:String = splitData![0]
        if identifierString == "SMSTO" {
            if (splitData?.count)! >= 3 {
                let smsNumber:String = splitData![1]
                let smsText:String = splitData![2]
                if ((smsNumber.characters.count > 0) || (smsText.characters.count > 0)) {
                    dataType = .SMS
                }
            }
        }
        else if identifierString == "tel" {
            if (splitData?.count)! >= 2 {
                let phoneNumber:String = splitData![1]
                let phoneURL = URL(string: "tel://" + phoneNumber)!
                if UIApplication.shared.canOpenURL(phoneURL) {
                    dataType = .Call
                }
            }
        }
        else if identifierString == "geo" {
            if (((splitData?.count)! >= 3) && ((splitData?[1].characters.count)! > 0) && ((splitData?[2].characters.count)! > 0)) {
                let latitude:Float = Float(splitData![1])!
                let longitude:Float = Float(splitData![2])!
                dataType = .GeoLocation
            }
        }
        else if identifierString == "MATMSG" {
            var emailStringSplit = capturedString.components(separatedBy: "MATMSG:TO:")
            if emailStringSplit.count == 2 {
                var stringPart: String = emailStringSplit[0] as String
                if stringPart.characters.count == 0 {
                    stringPart = emailStringSplit[1]
                    emailStringSplit = stringPart.components(separatedBy: ";SUB:")
                    if emailStringSplit.count > 1 {
                        let emailID: String = emailStringSplit[0]
                        emailStringSplit.remove(at: 0)
                        stringPart = emailStringSplit.joined(separator: ";SUB:")
                        emailStringSplit = stringPart.components(separatedBy: ";BODY:")
                        if emailStringSplit.count > 1 {
                            let emailSubject = emailStringSplit[0]
                            emailStringSplit.remove(at: 0)
                            stringPart = emailStringSplit.joined(separator: ";BODY:")
                            var emailBody = stringPart
                            emailStringSplit = stringPart.components(separatedBy: ";;")
                            if emailStringSplit.count > 0 {
                                emailStringSplit.removeLast()
                                emailBody = emailStringSplit.joined(separator: ";;")
                            }
                            dataType = .Email
                        }
                    }
                }
            }
        }
        else if identifierString == "BEGIN" {
            var tempArray = capturedString.components(separatedBy: "BEGIN:VEVENT")
            if tempArray.count > 1 {
                var summary:String?
                var dateStart:Date?
                var dateEnd:Date?
                tempArray.removeFirst()
                tempArray = tempArray.joined(separator: "BEGIN:VEVENT").components(separatedBy: "\n")
                for lineString:String in tempArray {
                    if ((lineString.characters.count == 0) || (lineString == "END:VEVENT")) {
                        continue
                    }
                    var eventTempArray = lineString.components(separatedBy: "SUMMARY:")
                    if eventTempArray.count > 1 {
                        eventTempArray.removeFirst()
                        summary = eventTempArray.joined(separator: "SUMMARY:")
                    }
                    
                    eventTempArray = lineString.components(separatedBy: "DTSTART:")
                    if eventTempArray.count > 1 {
                        eventTempArray.removeFirst()
                        dateStart = getDateFromVEventTimeStamp(theTimeStamp: eventTempArray.first!)
                    }
                    
                    eventTempArray = lineString.components(separatedBy: "DTEND:")
                    if eventTempArray.count > 1 {
                        eventTempArray.removeFirst()
                        dateEnd = getDateFromVEventTimeStamp(theTimeStamp: eventTempArray.first!)
                    }
                }
                if ((summary != nil) && (dateStart != nil) && (dateEnd != nil)) {
                    dataType = .Event
                }
            }
            
            tempArray = capturedString.components(separatedBy: "BEGIN:VCARD")
            if tempArray.count > 1 {
                let contactData:Data = capturedString.data(using: .utf8)!
                /*var contacts = [CNContact]()
                do{
                    contacts = try CNContactVCardSerialization.contacts(with: contactData)
                } catch {
                    print("vcardFromContacts \(error)")
                }
                if !contacts.isEmpty {
                    contacts.count
                }*/

                let adressBook: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeUnretainedValue()
                let source: ABRecord = ABAddressBookCopyDefaultSource(adressBook).takeUnretainedValue()
                let people: [ABRecord] = ABPersonCreatePeopleInSourceWithVCardRepresentation(source, contactData as CFData!).takeUnretainedValue() as [ABRecord]
                if ((people != nil) && (people.count > 0)) {
                    let name = "\(ABRecordCopyCompositeName(people.first).takeUnretainedValue())"
                }
                
                let vdata:Data = ABPersonCreateVCardRepresentationWithPeople(people as CFArray!).takeRetainedValue() as Data
                let vcardstring = String(data: vdata, encoding: .utf8)
            }
        }
    }
}

if dataType == .String {
    if let urlTial:URL = URL(string: capturedString) {
        if ((urlTial.scheme != nil) && (urlTial.host != nil)) {
            if UIApplication.shared.canOpenURL(urlTial) {
                dataType = .URL
            }
        }
    }
}

if dataType == .String {
    
}
dataType.rawValue

