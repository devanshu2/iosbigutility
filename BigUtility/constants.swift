//
//  constants.swift
//  BigUtility
//
//  Created by Devanshu Saini on 04/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit

public enum BarCodeDataType: String {
    case URL = "URL"
    case VCARD = "VCARD"
    case SMS = "SMS"
    case Call = "CALL"
    case GeoLocation = "GeoLocation"
    case Event = "Event"
    case Email = "Email"
    case Wifi = "Wifi"
    case String = "String"
}

struct Constants {
    struct ViewNames {
        static let MainStoryboard = "Main"
        static let IdentifierCodeReader = "qrreader"
        static let IdentifierCodeGenerator = "qrgenerator"
        static let IdentifierContainer = "containerController"
        static let IdentifierHome = "theHomeController"
        static let IdentifierTablePicker = "tablePickerController"
        static let IdentifierDrawer = "drawerController"
        static let IdentifierMapView = "BigMapViewController"
        static let IdentifierWebView = "BigWebViewController"
        static let IdentifierMoviePlayer = "movieplayer"
    }
    
    struct FireBase {
        static let AdUnitID = "ca-app-pub-7713409174367680/4274004138"
    }
    
    struct ConstantStrings {
        static let CodedImageFooterText = "MacroApps.com"
        static let ScreenTrackFirebaseKey = "ScreenName"
    }
    
    struct FontNames {
        static let Helvetica = "Helvetica"
        static let HelveticaBold = "Helvetica-Bold"
    }
    
    struct Fonts {
        static let BoldTextFont = UIFont(name: Constants.FontNames.HelveticaBold, size: Constants.FontSize.Text)
        static let TextFont = UIFont(name: Constants.FontNames.Helvetica, size: Constants.FontSize.Text)
        static let CodeAdTextFont = UIFont(name: Constants.FontNames.HelveticaBold, size: Constants.FontSize.CodeAdText)
        static let DefaultButtonFont = UIFont(name: Constants.FontNames.Helvetica, size: Constants.FontSize.Control)
    }
    
    struct FontSize {
        static let Control: CGFloat = 15.0
        static let Text: CGFloat = 14.0
        static let CodeAdText: CGFloat = 12.0
    }
    
    struct Colors {
        static let NavigationColor = UIColor(hex: "304269")
        static let LightBlueColor = UIColor(hex: "91BED4")
        static let LightLightBlueColor = UIColor(hex: "D9E8F5")
        static let WhiteColor = UIColor(hex: "FFFFFF")
        static let ButtonColor = UIColor(hex: "F26101")
        static let FilterImageTextColor = UIColor.darkGray
    }
    
    struct CellIdentifier {
        static let GeneratorTextViewCell = "GeneratorTextViewCell"
        static let GeneratorSelectionCell = "GeneratorSelectionCell"
        static let GeneratorTextFieldCell = "GeneratorTextFieldCell"
        static let GeneratorSMSCell = "GeneratorSMSCell"
        static let GeneratorEmailCell = "GeneratorEmailCell"
        static let TablePickerCell = "TablePickerCell"
    }
    
    struct Sounds {
        static let BeepSound = "beep"
    }
    
    struct General {
        static let Documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        static let Tmp = NSTemporaryDirectory()
        static let AdMobBigUtilityAdID = "ca-app-pub-7713409174367680~7367071336"
        static let AnimationDuration = 0.3
    }
    
    struct ImageNames {
        static let ScanCode = "qrcode-scan"
        static let GenerateCode = "qrcode"
        static let Check = "check"
    }
    
    struct GAScreenNames {
        static let HomeScreen = "Home Screen"
        static let CodeReaderScreen = "Code Reader Screen"
        static let CodeGeneratorScreen = "Code Generator Screen"
        static let CodeTypeScreen = "Select Code Type Screen"
        static let CodeTypeWebViewScreen = "Web View Screen"
        static let CodeTypeMapScreen = "Map Screen"
        static let CodeTypeMovieScreen = "Movie Screen"
    }
    
    struct ScreenNavigationTitle {
        static let HomeScreen = NSLocalizedString("Home", comment: "Home")
        static let CodeReaderScreen = NSLocalizedString("Code Reader", comment: "Code Reader")
        static let AztecCodeGeneratorScreen = NSLocalizedString("Aztec Code Generator", comment: "Aztec Code Generator")
        static let Code128BarCodeGeneratorScreen = NSLocalizedString("Code128Bar Code Generator", comment: "Code128Bar Code Generator")
        static let PDF417BarCodeGeneratorScreen = NSLocalizedString("PDF417Bar Code Generator", comment: "PDF417Bar Code Generator")
        static let QRCodeGeneratorScreen = NSLocalizedString("QR Code Generator", comment: "QR Code Generator")
        static let CodeTypeScreen = NSLocalizedString("Select Code Type", comment: "Select Code Type")
        static let MapScreen = NSLocalizedString("Map", comment: "Map")
    }
    
    struct CIScanCodeFiltersName {
        static let AztecCode = "CIAztecCodeGenerator"
        static let Barcode128 = "CICode128BarcodeGenerator"
        static let BarcodePDF417 = "CIPDF417BarcodeGenerator"
        static let QRCode = "CIQRCodeGenerator"
        /*
         AVMetadataObjectTypeUPCECode,
         AVMetadataObjectTypeCode39Code,
         AVMetadataObjectTypeCode39Mod43Code,
         AVMetadataObjectTypeEAN13Code,
         AVMetadataObjectTypeEAN8Code,
         AVMetadataObjectTypeCode93Code,
         AVMetadataObjectTypeCode128Code,
         AVMetadataObjectTypePDF417Code,
         AVMetadataObjectTypeQRCode,
         AVMetadataObjectTypeAztecCode
         */
    }
}
