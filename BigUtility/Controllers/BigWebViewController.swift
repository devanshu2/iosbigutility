//
//  BigWebViewController.swift
//  BigUtility
//
//  Created by Devanshu Saini on 29/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds

class BigWebViewController: BaseViewController, GADBannerViewDelegate, UITextFieldDelegate, UIWebViewDelegate {
    
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var addressWrapper: UIView!
    
    @IBOutlet weak var addressWrapperTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var advertisementView: GADBannerView!
    
    @IBOutlet weak var advertisementViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var advertisementViewHeightConstraint: NSLayoutConstraint!
    
    public var webViewURLString:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = Constants.GAScreenNames.CodeTypeWebViewScreen
        let goToolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 44.0))
        let toolSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let toolDone: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Go", comment: "Go"), style: .plain, target: self, action: #selector(self.toolGoAction(_:)))
        let toolCancel: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .plain, target: self, action: #selector(self.toolCancelAction(_:)))
        goToolbar.setItems([toolCancel, toolSpace, toolDone], animated: false)
        self.addressTextField.inputAccessoryView = goToolbar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigation()
        self.loadBannerAdvertisement(inView: advertisementView, andDelegate: self)
    }
    
    func toolCancelAction(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    func toolGoAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.webView.loadRequest(URLRequest(url: URL(string: self.addressTextField.text!)!))
    }
    
    fileprivate func setupNavigation() {
        let shareButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        shareButton.setTitleColor(Constants.Colors.NavigationColor, for: .normal)
        shareButton.setTitle(NSLocalizedString("Share", comment: "Share"), for: .normal)
        shareButton.addTarget(self, action: #selector(self.shareURL(_:)), for: .touchUpInside)
        let rightNavButton = UIBarButtonItem(customView: shareButton)
        navigationItem.rightBarButtonItem = rightNavButton
    }
    
    @objc fileprivate func shareURL(_ sender: AnyObject){
        DispatchQueue.main.async {
            guard ((self.webView.request?.url) != nil) else {
                if AppConfiguration.sharedInstance.isFireBaseEventLogEnabled() {
                    FIRAnalytics.logEvent(withName: "WebViewShareError", parameters: ["initital-url": self.webViewURLString! as NSObject])
                }
                return
            }
            let objectsToShare: [URL] = [(self.webView.request?.url)!]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = sender as? UIButton
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 25, y: 30, width: 1, height: 1)
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    //MARK: GADBannerViewDelegate Methods
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        self.setHiddenForAdvertisementView(forHeightConstraint: self.advertisementViewHeightConstraint, forBottomConstraint: self.advertisementViewBottomConstraint, setHidden: false, withAnimation: true)
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        Utils.print("Advertisement Error: \(error.localizedDescription)")
    }
    
    //MARK: WebView Delegates
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let pageTitle = webView.stringByEvaluatingJavaScript(from: "document.title")
        if pageTitle != nil {
            self.navigationItem.title = pageTitle!
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"), message: error.localizedDescription, preferredStyle: .alert);
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
        let retryAction = UIAlertAction(title: NSLocalizedString("Retry", comment: "Retry"), style: .default) { (theAction) in
            self.webView.reload()
        }
        alert.addAction(okAction)
        alert.addAction(retryAction)
        self.present(alert, animated: true, completion: nil)
    }
}
