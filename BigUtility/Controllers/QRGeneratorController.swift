//
//  QRGeneratorController.swift
//  Big Utility
//
//  Created by Devanshu Saini on 01/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds

public class GenerateCodeModal: NSObject {
    public var codeString: String?
    public var codeType: String!
    public var qrCodeType: BarCodeDataType = .String
    public var currentDataModel: BarCodeBaseData?
    
    override init() {
        super.init()
        self.codeType = Constants.CIScanCodeFiltersName.QRCode
        //self.qrCodeType = .String
    }
    
    convenience init(codeString:String, codeType: String) {
        self.init()
        self.codeType = codeType
        self.codeString = codeString
    }
    
    convenience init(codeString:String, codeType: String, qrCodeType: BarCodeDataType) {
        self.init()
        self.codeType = codeType
        self.codeString = codeString
        self.qrCodeType = qrCodeType
    }
    
    public func flushDataWithDefaults() {
        self.codeString = ""
        self.codeType = Constants.CIScanCodeFiltersName.QRCode
        self.qrCodeType = .String
    }
}

class QRGeneratorController: BaseViewController, TablePickerDelegate, UITextFieldDelegate, UITextViewDelegate, GADBannerViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Class Variables
    
    @IBOutlet weak var inputTable: UITableView!
    
    @IBOutlet weak var advertisementView: GADBannerView!
    
    @IBOutlet weak var advertisementViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var advertisementViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var filterImageView: UIImageView!
    
    @IBOutlet weak var generateButton: UIButton!
    
    @IBOutlet weak var newButton: UIButton!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var formWrapper: UIView!
    
    @IBOutlet weak var generatedFilterWrapper: UIView!
    
    var codedImage: CIImage!
    
    var isFormVisible = true
    
    private var codeModal: GenerateCodeModal!
    
    fileprivate var doneToolbar: UIToolbar!
    
    fileprivate var displayCodes: Array<String> = ["QR Code", "Aztec Code", "Code 128 Barcode", "PDF417 Barcode"]
    
    fileprivate var displayCodesID: Array<String> = [Constants.CIScanCodeFiltersName.QRCode, Constants.CIScanCodeFiltersName.AztecCode, Constants.CIScanCodeFiltersName.Barcode128, Constants.CIScanCodeFiltersName.BarcodePDF417]
    
    fileprivate var displayQRCodesID: Array<String> = [
        Utils.getLocalName(forBarCodeType: BarCodeDataType.String),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.URL),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.VCARD),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.SMS),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.Call),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.Email),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.GeoLocation),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.Event),
        Utils.getLocalName(forBarCodeType: BarCodeDataType.Wifi)]
    
    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = Constants.GAScreenNames.CodeGeneratorScreen
        self.generateButton.setThemeStyle()
        self.newButton.setThemeStyle()
        self.doneToolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 44.0))
        let toolSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let toolDone: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: "Done"), style: .plain, target: self, action: #selector(self.toolDoneAction(_:)))
        self.doneToolbar.setItems([toolSpace, toolDone], animated: false)
        self.codeModal = GenerateCodeModal(codeString: "", codeType: Constants.CIScanCodeFiltersName.QRCode)
        self.codeModal.currentDataModel = nil
        Utils.print(self.codeModal.qrCodeType)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigation(withShare: !self.isFormVisible)
        self.loadBannerAdvertisement(inView: advertisementView, andDelegate: self)
        Utils.print(self.codeModal.qrCodeType)
    }
    
    // MARK: Private Methods
    
    fileprivate func setupNavigation(withShare: Bool){
        var rightNavButton : UIBarButtonItem? = nil
        if withShare {
            let shareButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
            shareButton.setTitleColor(Constants.Colors.NavigationColor, for: .normal)
            shareButton.setTitle(NSLocalizedString("Share", comment: "Share"), for: .normal)
            shareButton.addTarget(self, action: #selector(self.shareQR(_:)), for: .touchUpInside)
            rightNavButton = UIBarButtonItem(customView: shareButton)
        }
        navigationItem.rightBarButtonItem = rightNavButton
        navigationItem.title = Constants.ScreenNavigationTitle.QRCodeGeneratorScreen
    }
    
    func toolDoneAction(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    fileprivate func animateTwoViews(_ newTopView: UIView, oldTopView: UIView, completionClosure: @escaping (_ success: Bool) -> Void){
        newTopView.alpha = 0.0
        newTopView.isHidden = false
        self.view.bringSubview(toFront: newTopView)
        newTopView.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        UIView.animate(withDuration: 0.3, animations: {
            newTopView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            newTopView.alpha = 1.0
            oldTopView.alpha = 0.0;
            }, completion: {
                (value: Bool) in
                if value {
                    oldTopView.isHidden = true
                    self.view.sendSubview(toBack: oldTopView)
                    completionClosure(value)
                }
        })
    }
    
    fileprivate func displayCodedImage() {
        let scaleX = self.filterImageView.frame.size.width / self.codedImage.extent.size.width
        let scaleY = self.filterImageView.frame.size.height / self.codedImage.extent.size.height
        
        let transformedImage = self.codedImage.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        if AppConfiguration.sharedInstance.isCodedImageAdvertisementWaterEnabled() {
            self.filterImageView.image = CIScanCodeFilters.sharedInstance.getWaterMarkedFilteredImage(transformedImage, bottomText: Constants.ConstantStrings.CodedImageFooterText, containerSize: self.filterImageView.frame.size)
        }
        else {
            self.filterImageView.image = UIImage(ciImage: transformedImage)
        }
    }
    
    fileprivate func getTitleOfTableHeader(forSection section: Int) -> String{
        var title: String = ""
        if section == 0 {
            title = NSLocalizedString("Select type of code image you want to generate.", comment: "Select type of code image you want to generate.")
        }
        else if section == 1 {
            if self.codeModal.codeType == Constants.CIScanCodeFiltersName.QRCode {
                title = NSLocalizedString("Select type of QR Code.", comment: "Select type of QR Code.")
            }
            else{
                title = NSLocalizedString("Provide data for code image.", comment: "Provide data for code image.")
            }
        }
        else if section == 2 {
            title = NSLocalizedString("Provide data for code image.", comment: "Provide data for code image.")
        }
        return title
    }
    
    // MARK: IBAction method implementation
    
    @IBAction func generateButtonAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        let trimmedString = self.codeModal.codeString?.trimmingCharacters(
            in: CharacterSet.whitespaces
        )
        if (trimmedString == "" || self.codeModal.codeType == "") {
            let alert: UIAlertController = UIAlertController(title: nil, message: "Provide some input first", preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        self.codedImage = CIScanCodeFilters.sharedInstance.getCodeImage(trimmedString!, codeType: self.codeModal.codeType)
        if self.codedImage != nil {
            self.animateTwoViews(self.generatedFilterWrapper, oldTopView: self.formWrapper ) { (success) in
                self.isFormVisible = false
                self.setupNavigation(withShare: !self.isFormVisible)
            }
            self.displayCodedImage()
        }
        else{
            let alert: UIAlertController = UIAlertController(title: nil, message: "Input seems invalid", preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func changeImageViewScale(_ sender: AnyObject) {
        self.filterImageView.transform = CGAffineTransform(scaleX: CGFloat(slider.value), y: CGFloat(slider.value))
    }
    
    @IBAction func newButtonAction(_ sender: AnyObject) {
        self.isFormVisible = true
        self.setupNavigation(withShare: !self.isFormVisible)
        self.codedImage = nil
        self.codeModal.flushDataWithDefaults()
        self.inputTable.reloadData()
        self.animateTwoViews(self.formWrapper, oldTopView: self.generatedFilterWrapper) { (success) in
            self.slider.setValue(1.0, animated: false)
            self.changeImageViewScale(self.slider)
            self.filterImageView.image = nil
        }
    }
    
    @objc fileprivate func shareQR(_ sender: AnyObject){
        DispatchQueue.main.async {
            if self.filterImageView.image != nil {
                let objectsToShare: [AnyObject] = [ self.filterImageView.image! ]
                let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [])
                activityViewController.popoverPresentationController?.sourceView = sender as? UIButton
                activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 25, y: 30, width: 1, height: 1)
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: TablePickerDelegate Methods
    
    func didCancelTablePicker() {
        
    }
    
    func didSuccessSelectionTablePicker(withSelection selection: Set<Int>, andArbitraryIndex theIndex: Int) {
        if theIndex == 0 {
            self.codeModal.codeType = ""
            if selection.count > 0 {
                self.codeModal.codeType = self.displayCodesID[Int(selection.first!)]
                self.codeModal.currentDataModel = nil
            }
        }
        else if theIndex == 1 {
            self.codeModal.qrCodeType = .String
            if selection.count > 0 {
                switch Int(selection.first!) {
                case 1:
                    self.codeModal.qrCodeType = .URL
                    self.codeModal.currentDataModel = BarCodeURLData()
                case 2:
                    self.codeModal.qrCodeType = .VCARD
                    self.codeModal.currentDataModel = BarCodeVCardData()
                case 3:
                    self.codeModal.qrCodeType = .SMS
                    self.codeModal.currentDataModel = BarCodeSMSData()
                case 4:
                    self.codeModal.qrCodeType = .Call
                    self.codeModal.currentDataModel = BarCodeTelData()
                case 5:
                    self.codeModal.qrCodeType = .Email
                    self.codeModal.currentDataModel = BarCodeEmailData()
                case 6:
                    self.codeModal.qrCodeType = .GeoLocation
                    self.codeModal.currentDataModel = BarCodeGeoData()
                case 7:
                    self.codeModal.qrCodeType = .Event
                    self.codeModal.currentDataModel = BarCodeVEventData()
                case 8:
                    self.codeModal.qrCodeType = .Wifi
                    self.codeModal.currentDataModel = BarCodeWifiData()
                default:
                    self.codeModal.qrCodeType = .String
                    self.codeModal.currentDataModel = nil
                }
            }
        }
        self.inputTable.reloadData()
    }
    
    //MARK: UITextViewDelegate Methods
    
    func textViewDidChange(_ textView: UITextView) {
        if self.codeModal.qrCodeType == .SMS {
            (self.codeModal.currentDataModel as! BarCodeSMSData).smsText = textView.text
        }
        else if self.codeModal.qrCodeType == .Email {
            (self.codeModal.currentDataModel as! BarCodeEmailData).message = textView.text
        }
        else {
            self.codeModal.codeString = textView.text
        }
    }
    
    //MARK: UITextFieldDelegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.addObserver(self, selector: #selector(UITextFieldTextDidChange(_:)), name: NSNotification.Name.UITextFieldTextDidChange, object: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if self.codeModal.qrCodeType == .GeoLocation {
            let text = textField.text! + string
            if text != "-" {
                let someString = Double(text)
                if someString == nil {
                    return false
                }
            }
        }
        return true
    }
    
    func UITextFieldTextDidChange(_ notification:Notification) {
        let textfield:UITextField = notification.object as! UITextField;
        let text = textfield.text;
        if self.codeModal.qrCodeType == .URL {
            (self.codeModal.currentDataModel as! BarCodeURLData).theURL = text
        }
        else if self.codeModal.qrCodeType == .SMS {
            (self.codeModal.currentDataModel as! BarCodeSMSData).smsText = text
        }
        else if self.codeModal.qrCodeType == .Call {
            (self.codeModal.currentDataModel as! BarCodeTelData).telephoneNumber = text
        }
        else if self.codeModal.qrCodeType == .GeoLocation {
            if textfield.tag == 1 {
                 (self.codeModal.currentDataModel as! BarCodeGeoData).latitude = Double(text!)
            }
            else if textfield.tag == 2 {
                (self.codeModal.currentDataModel as! BarCodeGeoData).longitude = Double(text!)
            }
        }
        else if self.codeModal.qrCodeType == .Event {
            if textfield.tag == 1 {
                (self.codeModal.currentDataModel as! BarCodeVEventData).summary = text
            }
        }
        else if self.codeModal.qrCodeType == .Email {
            if textfield.tag == 1 {
                (self.codeModal.currentDataModel as! BarCodeEmailData).emailID = text
            }
            else if textfield.tag == 2 {
                (self.codeModal.currentDataModel as! BarCodeEmailData).subject = text
            }
        }
        else if self.codeModal.qrCodeType == .Wifi {
            if textfield.tag == 1 {
                (self.codeModal.currentDataModel as! BarCodeWifiData).SSID = text
            }
            else if textfield.tag == 2 {
                (self.codeModal.currentDataModel as! BarCodeWifiData).password = text
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidChange, object: textField)
    }
    
    /*@available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
    }*/
    
    //MARK: GADBannerViewDelegate Methods
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        self.setHiddenForAdvertisementView(forHeightConstraint: self.advertisementViewHeightConstraint, forBottomConstraint: self.advertisementViewBottomConstraint, setHidden: false, withAnimation: true)
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        Utils.print("Advertisement Error: \(error.localizedDescription)")
    }

    //MARK: UITableViewDataSource Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Utils.print(self.codeModal.qrCodeType)
        var cell: UITableViewCell!
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorSelectionCell)
            let titleLabel: UILabel = cell.viewWithTag(1) as! UILabel
            titleLabel.text = NSLocalizedString("Select Type", comment: "Select Type")
            titleLabel.font = Constants.Fonts.BoldTextFont
            let selectionLabel: UILabel = cell.viewWithTag(2) as! UILabel
            var selectionText: String = ""
            if self.codeModal.codeType.length > 0 {
                if let foundIndex: Int = self.displayCodesID.index(of: self.codeModal.codeType) {
                    selectionText = self.displayCodes[foundIndex]
                }
            }
            selectionLabel.text = selectionText
            selectionLabel.font = Constants.Fonts.TextFont
        }
        else {
            if self.codeModal.codeType != Constants.CIScanCodeFiltersName.QRCode {
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorTextViewCell)
                let textView: UITextView = cell.viewWithTag(1) as! UITextView
                textView.delegate = self
                textView.text = self.codeModal.codeString
                textView.font = Constants.Fonts.TextFont
                textView.inputAccessoryView = doneToolbar
                textView.layer.borderColor = UIColor.lightGray.cgColor
                textView.layer.borderWidth = 1.0
                textView.layer.cornerRadius = 5.0
            }
            else {
                if indexPath.section == 1 {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorSelectionCell)
                    let titleLabel: UILabel = cell.viewWithTag(1) as! UILabel
                    titleLabel.text = NSLocalizedString("Select Qr Code Type", comment: "Select Qr Code Type")
                    titleLabel.font = Constants.Fonts.BoldTextFont
                    let selectionLabel: UILabel = cell.viewWithTag(2) as! UILabel
                    selectionLabel.text = Utils.getLocalName(forBarCodeType: self.codeModal.qrCodeType)
                    selectionLabel.font = Constants.Fonts.TextFont
                }
                else if indexPath.section == 2 {
                    switch self.codeModal.qrCodeType {
                    case .URL:
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorTextFieldCell)
                        let texfield: UITextField = cell.viewWithTag(1) as! UITextField
                        texfield.delegate = self
                        texfield.text = "its url"//(self.codeModal.currentDataModel as! BarCodeURLData).theURL
                        texfield.keyboardType = .URL
                        texfield.font = Constants.Fonts.TextFont
                        texfield.inputAccessoryView = doneToolbar
                        texfield.layer.borderColor = UIColor.lightGray.cgColor
                        texfield.layer.borderWidth = 1.0
                        texfield.layer.cornerRadius = 5.0
                    case .VCARD:
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorSelectionCell)
                        let titleLabel: UILabel = cell.viewWithTag(1) as! UILabel
                        titleLabel.text = NSLocalizedString("Select Contact", comment: "Select Contact")
                        titleLabel.font = Constants.Fonts.BoldTextFont
                        let selectionLabel: UILabel = cell.viewWithTag(2) as! UILabel
                        selectionLabel.text = "Add date here"
                        selectionLabel.font = Constants.Fonts.TextFont
                    case .SMS:
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorSMSCell)
                        let texfield: UITextField = cell.viewWithTag(1) as! UITextField
                        texfield.delegate = self
                        texfield.text = (self.codeModal.currentDataModel as! BarCodeSMSData).smsNumber
                        texfield.font = Constants.Fonts.TextFont
                        texfield.inputAccessoryView = doneToolbar
                        texfield.layer.borderColor = UIColor.lightGray.cgColor
                        texfield.layer.borderWidth = 1.0
                        texfield.layer.cornerRadius = 5.0
                        
                        let texview: UITextView = cell.viewWithTag(2) as! UITextView
                        texview.delegate = self
                        texview.text = (self.codeModal.currentDataModel as! BarCodeSMSData).smsText
                        texview.font = Constants.Fonts.TextFont
                        texview.inputAccessoryView = doneToolbar
                        texview.layer.borderColor = UIColor.lightGray.cgColor
                        texview.layer.borderWidth = 1.0
                        texview.layer.cornerRadius = 5.0
                    case .Call:
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorTextFieldCell)
                        let texfield: UITextField = cell.viewWithTag(1) as! UITextField
                        texfield.delegate = self
                        texfield.text = (self.codeModal.currentDataModel as! BarCodeTelData).telephoneNumber
                        texfield.keyboardType = .phonePad
                        texfield.font = Constants.Fonts.TextFont
                        texfield.inputAccessoryView = doneToolbar
                        texfield.layer.borderColor = UIColor.lightGray.cgColor
                        texfield.layer.borderWidth = 1.0
                        texfield.layer.cornerRadius = 5.0
                    case .GeoLocation:
                        Utils.print("Unhandled")
                    case .Event:
                        Utils.print("Unhandled")
                    case .Email:
                        Utils.print("Unhandled")
                    case .Wifi:
                        Utils.print("Unhandled")
                    default:
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.GeneratorTextViewCell)
                        let textView: UITextView = cell.viewWithTag(1) as! UITextView
                        textView.delegate = self
                        textView.text = self.codeModal.codeString
                        textView.font = Constants.Fonts.TextFont
                        textView.inputAccessoryView = doneToolbar
                        textView.layer.borderColor = UIColor.lightGray.cgColor
                        textView.layer.borderWidth = 1.0
                        textView.layer.cornerRadius = 5.0
                    }
                }
            }
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.codeModal.codeType == Constants.CIScanCodeFiltersName.QRCode {
            return 3
        }
        else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.getTitleOfTableHeader(forSection: section)
    }

    //MARK: UITableViewDelegate Methods

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Utils.print(self.codeModal.qrCodeType)
        if indexPath.section == 0 {
            return 50.0
        }
        else {
            if self.codeModal.codeType != Constants.CIScanCodeFiltersName.QRCode {
                return 80.0
            }
            else {
                if indexPath.section == 1 {
                    return 50.0
                }
                else if indexPath.section == 2 {
                    if self.codeModal.qrCodeType == .URL {
                        return 46.0
                    }
                    else if self.codeModal.qrCodeType == .VCARD {
                        return 50.0
                    }
                    else if self.codeModal.qrCodeType == .SMS {
                        return 150.0
                    }
                    else if self.codeModal.qrCodeType == .Call {
                        return 46.0
                    }
                    else if self.codeModal.qrCodeType == .GeoLocation {
                        
                    }
                    else if self.codeModal.qrCodeType == .Event {
                        
                    }
                    else if self.codeModal.qrCodeType == .Email {
                        
                    }
                    else if self.codeModal.qrCodeType == .Wifi {
                        
                    }
                    else if self.codeModal.qrCodeType == .String {
                        return 100.0
                    }
                }
            }
        }
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section < 2 {
            let storyboard = UIStoryboard(name: Constants.ViewNames.MainStoryboard, bundle: nil)
            let dataPicker = storyboard.instantiateViewController(withIdentifier: Constants.ViewNames.IdentifierTablePicker) as! TablePicker
            dataPicker.allowMultipleSelection = false
            dataPicker.delegate = self
            dataPicker.isModal = false
            dataPicker.arbitraryIndex = indexPath.section
            dataPicker.selectedItemIndices = Set()
            if indexPath.row == 0 {
                dataPicker.selectedItemIndices?.insert(self.displayCodesID.index(of: self.codeModal.codeType)!)
                dataPicker.listItems = self.displayCodes
            }
            else if indexPath.row == 1 {
                dataPicker.selectedItemIndices?.insert(self.displayQRCodesID.index(of: Utils.getLocalName(forBarCodeType: self.codeModal.qrCodeType))!)
                dataPicker.listItems = self.displayQRCodesID
            }
            self.navigationController?.pushViewController(dataPicker, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if view.isKind(of: UITableViewHeaderFooterView.self) {
            let theHeaderView: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
            theHeaderView.textLabel?.text = self.getTitleOfTableHeader(forSection: section)
        }
    }
}
