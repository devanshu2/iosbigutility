//
//  BaseViewController.swift
//  Big Utility
//
//  Created by Devanshu Saini on 01/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds

class BaseViewController: UIViewController {
    var screenName: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.screenName == nil {
            self.screenName = NSStringFromClass(type(of: self))
        }
        if AppConfiguration.sharedInstance.isFireBaseEventLogEnabled() {
            FIRAnalytics.logEvent(withName: Constants.ConstantStrings.ScreenTrackFirebaseKey, parameters: ["name": self.screenName! as NSObject])
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Utils.print("Memory warning")
    }
    
    public func loadBannerAdvertisement(inView adView: GADBannerView, andDelegate delegate:GADBannerViewDelegate) {
        if AppConfiguration.sharedInstance.isAdmobEnabled() == false {
            return
        }
        adView.adSize = kGADAdSizeSmartBannerPortrait
        adView.rootViewController = self
        adView.delegate = delegate
        let adRequest: GADRequest = GADRequest()
        #if DEBUG
            adView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            adRequest.testDevices = ["2077ef9a63d2b398840261c8221a0c9b"]
        #else
            adView.adUnitID = Constants.FireBase.AdUnitID
        #endif
        adView.load(adRequest)
    }
    
    public func setHiddenForAdvertisementView(forHeightConstraint heightContraint:NSLayoutConstraint, forBottomConstraint bottomContraint:NSLayoutConstraint, setHidden hidden: Bool, withAnimation animation: Bool) {
        heightContraint.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 90.0 : 50.0
        bottomContraint.constant = hidden ? ( -1 ) * heightContraint.constant : 0.0
        if animation {
            UIView.animate(withDuration: Constants.General.AnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
        else {
            self.view.layoutIfNeeded()
        }
    }
}
