//
//  BigMapViewController.swift
//  BigUtility
//
//  Created by Devanshu Saini on 29/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import MapKit
import CoreLocation

//class BigMapAnnotation: NSObject, MKAnnotation {
//    public var coordinate:CLLocationCoordinate2D!
//    
//    public var userLocation:CLLocationCoordinate2D!
//    
//    convenience init(WithLocation theLocation:CLLocationCoordinate2D) {
//        self.init()
//        self.coordinate = theLocation
//    }
//    
//    public var title:String? = {
//        return "Location"
//    }()
//    
//    public var subtitle:String? = {
//        return ""
//    }()
//    
//    public static func createViewAnnotation(ForMapView mapView:MKMapView, andAnnotation annotation:MKAnnotation ) ->MKAnnotationView {
//        var returnedAnnotationView:MKAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "BigMapAnnotation")
//        if (returnedAnnotationView == nil) {
//            returnedAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "BigMapAnnotation")
//            returnedAnnotationView?.canShowCallout = true
//            returnedAnnotationView?.centerOffset = CGPoint(x: (returnedAnnotationView?.centerOffset.x)! + (returnedAnnotationView?.image?.size.width)!/2, y: (returnedAnnotationView?.centerOffset.y)! - (returnedAnnotationView?.image?.size.height)!/2)
//        }
//        else {
//            returnedAnnotationView?.annotation = annotation
//        }
//        return returnedAnnotationView!
//    }
//}

class BigMapViewController: BaseViewController, GADBannerViewDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var advertisementView: GADBannerView!
    
    @IBOutlet weak var advertisementViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var advertisementViewHeightConstraint: NSLayoutConstraint!
    
    public var userLocation:CLLocationCoordinate2D!
    
    private var isGoogleMapsAppAvailable:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = Constants.GAScreenNames.CodeTypeMapScreen
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigation()
        self.loadBannerAdvertisement(inView: advertisementView, andDelegate: self)
        self.isGoogleMapsAppAvailable = UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)
        self.mapView.setCenter(self.userLocation, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = self.userLocation
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude), completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                Utils.print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm:CLPlacemark = (placemarks?.first)!
                
                // not all places have thoroughfare & subThoroughfare so validate those values
                annotation.title = pm.thoroughfare! + ", " + pm.subThoroughfare!
                annotation.subtitle = pm.subLocality
                self.mapView.addAnnotation(annotation)
                Utils.print(pm)
            }
            else {
                annotation.title = "Unknown Place"
                self.mapView.addAnnotation(annotation)
                Utils.print("Problem with the data received from geocoder")
            }
        })
    }
    
    fileprivate func setupNavigation() {
        let shareButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        shareButton.setTitleColor(Constants.Colors.NavigationColor, for: .normal)
        shareButton.setTitle(NSLocalizedString("Share", comment: "Share"), for: .normal)
        shareButton.addTarget(self, action: #selector(self.shareCoordinates(_:)), for: .touchUpInside)
        let rightNavButton = UIBarButtonItem(customView: shareButton)
        navigationItem.rightBarButtonItem = rightNavButton
        navigationItem.title = Constants.ScreenNavigationTitle.MapScreen
    }
    
    @objc fileprivate func shareCoordinates(_ sender: AnyObject){
        let googleShareURLString = "http://www.google.com/maps/place/" + String(self.userLocation.latitude) + "," + String(self.userLocation.longitude)
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet);
        
        let shareOnAppleMaps = UIAlertAction(title: NSLocalizedString("Open in Apple Maps", comment: "Open in Apple Maps"), style: .default) { (theAction) in
            let placeMark = MKPlacemark(coordinate: self.userLocation, addressDictionary: nil)
            let destination = MKMapItem(placemark: placeMark)
            destination.openInMaps(launchOptions: nil)
        }
        alert.addAction(shareOnAppleMaps)
        
        if self.isGoogleMapsAppAvailable {
            let shareOnGoogleMaps = UIAlertAction(title: NSLocalizedString("Open in Google Maps", comment: "Open in Google Maps"), style: .default) { (theAction) in
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    UIApplication.shared.openURL(URL(string:
                        "comgooglemaps://?center=" + String(self.userLocation.latitude) + "," + String(self.userLocation.longitude) + "&zoom=14&views=traffic")!)
                } else {
                    print("Can't use comgooglemaps://");
                }
            }
            alert.addAction(shareOnGoogleMaps)
        }
        
        let shareAction = UIAlertAction(title: NSLocalizedString("Share", comment: "Share"), style: .default) { (theAction) in
            let objectsToShare: [String] = [ googleShareURLString ]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = sender as? UIButton
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 25, y: 30, width: 1, height: 1)
            self.present(activityViewController, animated: true, completion: nil)
        }
        alert.addAction(shareAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: GADBannerViewDelegate Methods
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        self.setHiddenForAdvertisementView(forHeightConstraint: self.advertisementViewHeightConstraint, forBottomConstraint: self.advertisementViewBottomConstraint, setHidden: false, withAnimation: true)
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        Utils.print("Advertisement Error: \(error.localizedDescription)")
    }
    
    //MARK: MapView Delegates
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        
    }
    
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"), message: error.localizedDescription, preferredStyle: .alert);
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
