//
//  BigMoviePlayerController.swift
//  BigUtility
//
//  Created by Devanshu Saini on 05/10/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import GoogleMobileAds

class BigMoviePlayerController: BaseViewController, UITextFieldDelegate, GADBannerViewDelegate {
    
    @IBOutlet weak var advertisementView: GADBannerView!
    
    @IBOutlet weak var advertisementViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var advertisementViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var canvasView:UIView!
    
    public var movieURL:URL?
    
    private var movieAsset:AVAsset?
    private var movieItem:AVPlayerItem?
    private var moviePlayer:AVPlayer?
    private var movieLayer:AVPlayerLayer?
    
    private var provideURLAlertOkAction:UIAlertAction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = Constants.GAScreenNames.CodeTypeMovieScreen
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.playerLayerDoubleTapGesture(theGesture:)))
        doubleTapGesture.numberOfTapsRequired = 2
        self.canvasView.addGestureRecognizer(doubleTapGesture)
        
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.playerLayerSwipeGesture(theGesture:)))
        swipeLeftGesture.direction = .left
        self.canvasView.addGestureRecognizer(swipeLeftGesture)
        
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.playerLayerSwipeGesture(theGesture:)))
        swipeRightGesture.direction = .right
        self.canvasView.addGestureRecognizer(swipeRightGesture)
        
        let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.playerLayerSwipeGesture(theGesture:)))
        swipeUpGesture.direction = .up
        self.canvasView.addGestureRecognizer(swipeUpGesture)
        
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.playerLayerSwipeGesture(theGesture:)))
        swipeDownGesture.direction = .down
        self.canvasView.addGestureRecognizer(swipeDownGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationGoesInBackground(_:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupNavigation()
        self.loadBannerAdvertisement(inView: advertisementView, andDelegate: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.moviePlayer != nil {
            self.moviePlayer?.pause()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if self.moviePlayer != nil {
            self.moviePlayer?.removeObserver(self, forKeyPath: "status", context: nil)
        }
    }
    
    fileprivate func setupNavigation() {
        let shareButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        shareButton.setTitleColor(Constants.Colors.NavigationColor, for: .normal)
        shareButton.setTitle(NSLocalizedString("Share", comment: "Share"), for: .normal)
        shareButton.addTarget(self, action: #selector(self.shareURL(_:)), for: .touchUpInside)
        let rightNavButton = UIBarButtonItem(customView: shareButton)
        navigationItem.rightBarButtonItem = rightNavButton
//        navigationItem.title = Constants.ScreenNavigationTitle.MapScreen
    }
    
    @objc private func applicationGoesInBackground(_ notification:NSNotification) {
        if self.moviePlayer != nil {
            self.moviePlayer?.pause()
        }
    }
    
    @objc fileprivate func shareURL(_ sender: AnyObject){
        DispatchQueue.main.async {
            guard (self.movieURL != nil) else {
                return
            }
            let objectsToShare: [URL] = [self.movieURL!]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = sender as? UIButton
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 25, y: 30, width: 1, height: 1)
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    private func playMovie() {
        if (self.movieURL == nil || self.movieURL?.scheme == nil || self.movieURL?.host == nil) {
            self.showInvalidURLAlert()
            return
        }
        if self.moviePlayer != nil {
            self.moviePlayer?.removeObserver(self, forKeyPath: "status", context: nil)
            self.moviePlayer?.pause()
        }
        if self.movieLayer != nil {
            self.movieLayer?.removeFromSuperlayer()
            self.movieLayer = nil
        }
        
        self.movieAsset = AVAsset(url: self.movieURL!)
        self.movieItem = AVPlayerItem(asset: self.movieAsset!)
        self.moviePlayer = AVPlayer(playerItem: self.movieItem)
        self.moviePlayer?.actionAtItemEnd = .pause
        
        self.movieLayer = AVPlayerLayer.init(player: self.moviePlayer)
        self.movieLayer?.frame = self.canvasView.bounds
        self.movieLayer?.videoGravity = AVLayerVideoGravityResizeAspect
        self.canvasView.layer.addSublayer(self.movieLayer!)
        
        self.moviePlayer?.addObserver(self, forKeyPath: "status", options: .new, context: nil)
        self.moviePlayer?.play()
    }
    
    func playerLayerDoubleTapGesture(theGesture gesture: UITapGestureRecognizer) {
        guard self.movieLayer != nil else {
            return
        }
        print("double tap")
    }
    
    func playerLayerSwipeGesture(theGesture gesture: UIGestureRecognizer) {
        guard self.movieLayer != nil else {
            return
        }
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
    }
    
    private func showInvalidURLAlert() {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Invalid Movie URL", comment: "Invalid Movie URL"), preferredStyle: .alert)
        let provideAction = UIAlertAction(title: NSLocalizedString("Change URL", comment: "Change URL"), style: .default) { (theAction) in
            self.provideURLAlert()
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(provideAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func provideURLAlert() {
        let alert = UIAlertController(title: NSLocalizedString("Movie URL", comment: "Movie URL"), message: NSLocalizedString("Input Valid Movie URL", comment: "Input Valid Movie URL"), preferredStyle: .alert)
        self.provideURLAlertOkAction = UIAlertAction(title: NSLocalizedString("Play", comment: "Play"), style: .default) { (theAction) in
            let urlTextField = alert.textFields![0] as UITextField
            self.movieURL = URL(string: urlTextField.text!)
            self.playMovie()
        }
        self.provideURLAlertOkAction?.isEnabled = false
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .cancel, handler: nil)
        alert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = NSLocalizedString("Movie URL", comment: "Movie URL")
            textField.delegate = self
            textField.keyboardType = .URL
            textField.returnKeyType = .done
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            if self.movieURL != nil {
                textField.text = self.movieURL?.absoluteString
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(self.provideURLAlertOkAction!)
        self.present(alert, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.provideURLAlertOkAction != nil {
            self.provideURLAlertOkAction?.isEnabled = false
            let inputString = textField.text?.appending(string)
            if Utils.isValidURLString(urlString: inputString!) {
                self.provideURLAlertOkAction?.isEnabled = true
            }
        }
        return true
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        self.setHiddenForAdvertisementView(forHeightConstraint: self.advertisementViewHeightConstraint, forBottomConstraint: self.advertisementViewBottomConstraint, setHidden: false, withAnimation: true)
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        Utils.print("Advertisement Error: \(error.localizedDescription)")
    }
}
