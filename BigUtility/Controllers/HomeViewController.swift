//
//  HomeViewController.swift
//  Big Utility
//
//  Created by Devanshu Saini on 28/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Firebase
import GoogleMobileAds

class HomeViewController: BaseViewController, GADBannerViewDelegate {
    
    @IBOutlet weak var theTableView: UITableView!
    
    @IBOutlet weak var advertisementView: GADBannerView!
    
    @IBOutlet weak var advertisementViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var advertisementViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        theTableView.registerCellNib(HomeTableCell.self)
        theTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        theTableView.backgroundColor = Constants.Colors.LightBlueColor
        self.screenName = Constants.GAScreenNames.HomeScreen
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
        self.loadBannerAdvertisement(inView: advertisementView, andDelegate: self)
    }
    
    fileprivate func setupNavigation(){
//        let leftNavButton : UIBarButtonItem = UIBarButtonItem(title: "Menu", style: .Plain, target: self, action: #selector(self.toggleLeft))
//        navigationItem.leftBarButtonItem = leftNavButton
        navigationItem.title = Constants.ScreenNavigationTitle.HomeScreen
    }
    
    fileprivate func setHiddenForAdvertisementView(setHidden hidden: Bool, withAnimation animation: Bool) {
        self.advertisementViewHeightConstraint.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 90.0 : 50.0
        self.advertisementViewBottomConstraint.constant = hidden ? ( -1 ) * self.advertisementViewHeightConstraint.constant : 0.0
        if animation {
            UIView.animate(withDuration: Constants.General.AnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
        else {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: GADBannerViewDelegate Methods
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        self.setHiddenForAdvertisementView(forHeightConstraint: self.advertisementViewHeightConstraint, forBottomConstraint: self.advertisementViewBottomConstraint, setHidden: false, withAnimation: true)
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        Utils.print("Advertisement Error: \(error.localizedDescription)")
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0.01
    }
}

extension HomeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier : String = String.className(HomeTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! HomeTableCell
        var cellLabelText: String = ""
        if (indexPath as NSIndexPath).row == 0 {
            cellLabelText = "Scan QR Code"
            cell.iconImageView.image = UIImage(named: Constants.ImageNames.ScanCode)
        }
        else if (indexPath as NSIndexPath).row == 1{
            cellLabelText = "Generate QR Code"
            cell.iconImageView.image = UIImage(named: Constants.ImageNames.GenerateCode)
        }
        cell.theLabel.text = cellLabelText
        cell.delegate = self
        cell.indexPath = indexPath
        cell.selectionStyle = .none
        if (indexPath as NSIndexPath).row % 2 == 0 {
            cell.backgroundColor = Constants.Colors.LightBlueColor
            cell.bottomView.backgroundColor = Constants.Colors.LightLightBlueColor
            cell.fluidView.fillColor = Constants.Colors.LightLightBlueColor
        }
        else{
            cell.backgroundColor = Constants.Colors.LightLightBlueColor
            cell.bottomView.backgroundColor = Constants.Colors.LightBlueColor
            cell.fluidView.fillColor = Constants.Colors.LightBlueColor
        }
        return cell
    }
}

extension HomeViewController : HomeTableCellDelegate{
    func gotTapOnHomeTableCell(indexPath aIndexPath:IndexPath){
        let storyboard = UIStoryboard(name: Constants.ViewNames.MainStoryboard, bundle: nil)
        var theViewController: UIViewController? = nil
        if (aIndexPath as NSIndexPath).row == 0 {
            theViewController = storyboard.instantiateViewController(withIdentifier: Constants.ViewNames.IdentifierCodeReader) as! QRReaderController
        }
        else if (aIndexPath as NSIndexPath).row == 1{
            theViewController = storyboard.instantiateViewController(withIdentifier: Constants.ViewNames.IdentifierCodeGenerator) as! QRGeneratorController
        }
        if theViewController != nil {
            self.navigationController?.pushViewController(theViewController!, animated: true)
        }
        else{
            #if DEBUG
                let alert: UIAlertController = UIAlertController(title: nil, message: "Not assigned", preferredStyle: .alert)
                let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            #endif
        }
        
    }
}

extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        Utils.print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        Utils.print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        Utils.print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        Utils.print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        Utils.print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        Utils.print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        Utils.print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        Utils.print("SlideMenuControllerDelegate: rightDidClose")
    }
}
