//
//  QRReaderController.swift
//  Big Utility
//
//  Created by Devanshu Saini on 01/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import AVFoundation

class QRReaderController: BaseViewController {
    
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    
    fileprivate var captureSession: AVCaptureSession!
    fileprivate var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    fileprivate var audioPlayer: AVAudioPlayer!
    fileprivate var isReading: Bool!
    fileprivate var dataHandler: BarcodeDataHandler!
    fileprivate var allowedCodeTypes = [AVMetadataObjectTypeUPCECode,
                                AVMetadataObjectTypeCode39Code,
                                AVMetadataObjectTypeCode39Mod43Code,
                                AVMetadataObjectTypeEAN13Code,
                                AVMetadataObjectTypeEAN8Code,
                                AVMetadataObjectTypeCode93Code,
                                AVMetadataObjectTypeCode128Code,
                                AVMetadataObjectTypePDF417Code,
                                AVMetadataObjectTypeQRCode,
                                AVMetadataObjectTypeAztecCode,
                                AVMetadataObjectTypeDataMatrixCode]
    private var captureDevice: AVCaptureDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.captureSession = nil
        self.isReading = false
        loadBeepSound()
        self.screenName = Constants.GAScreenNames.CodeReaderScreen
        dataHandler = BarcodeDataHandler(classDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    
    fileprivate func setupNavigation(){
        navigationItem.title = Constants.ScreenNavigationTitle.CodeReaderScreen
    }
    
    @IBAction func startStopReading(_ sender: AnyObject){
        if self.isReading == true {
            stopReading()
            self.startStopButton.setTitle("Start", for: UIControlState())
            self.isReading = false
        }
        else{
            self.isReading = self.startReading()
            if self.isReading == true {
                self.startStopButton.setTitle("Stop", for: UIControlState())
                self.lblStatus.text = "Scanning for QR Code..."
            }
        }
    }
    
    fileprivate func startReading() -> Bool{
        captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if captureDevice == nil {
            let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"), message: NSLocalizedString("Oops! No Camera Found.", comment: "Oops! No Camera Found."), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        var input: AVCaptureDeviceInput!
        do {
            try input = AVCaptureDeviceInput.init(device: captureDevice)
        } catch let error as NSError {
            Utils.print ("Error: \(error.localizedDescription)")
            return false
        }
        
        self.captureSession = AVCaptureSession()
        self.captureSession.addInput(input)
        
        let captureMetadataOutput: AVCaptureMetadataOutput = AVCaptureMetadataOutput()
        self.captureSession.addOutput(captureMetadataOutput)
        
        var newDispatchQueue: DispatchQueue!
        newDispatchQueue = DispatchQueue(label: "myQueue", attributes: [])
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: newDispatchQueue)
        captureMetadataOutput.metadataObjectTypes = self.allowedCodeTypes
        
        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.videoPreviewLayer.frame = self.viewPreview.layer.bounds
        self.viewPreview.layer.addSublayer(self.videoPreviewLayer)
        
        self.captureSession.startRunning()
        return true
    }
    
    fileprivate func stopReading(){
        self.captureSession.stopRunning()
        self.captureSession = nil
        self.videoPreviewLayer.removeFromSuperlayer()
    }
    
    fileprivate func loadBeepSound(){
        let beepFilePath: String! = Bundle.main.path(forResource: Constants.Sounds.BeepSound, ofType: "mp3")
        let beepURL: URL! = URL(fileURLWithPath: beepFilePath)
        do {
            try self.audioPlayer = AVAudioPlayer(contentsOf: beepURL)
            self.audioPlayer.prepareToPlay()
        } catch let error as NSError {
            Utils.print ("Error: \(error.localizedDescription)")
        }
    }
    
    fileprivate func handleCaptureData(_ capturedString: String?){
        guard capturedString != nil else {
            return
        }
        self.lblStatus.text = capturedString
        self.stopReading()
        self.startStopButton.setTitle("Start", for: UIControlState())
        self.isReading = false
        if self.audioPlayer != nil {
            self.audioPlayer.play()
        }
    }
}

extension QRReaderController: AVCaptureMetadataOutputObjectsDelegate{
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        if (metadataObjects != nil && metadataObjects.count > 0) {
            if let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject{
                DispatchQueue.main.async(execute: {
                    if metadataObj.isKind(of: AVMetadataMachineReadableCodeObject.self) {
                        Utils.print("Code Type: \(metadataObj.type)")
                        if metadataObj.type ==  AVMetadataObjectTypeUPCECode{
                            self.dataHandler.handleCapturedUPCECodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeCode39Code{
                            self.dataHandler.handleCaptured39CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeCode39Mod43Code{
                            self.dataHandler.handleCaptured39Mod43CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeEAN13Code{
                            self.dataHandler.handleCapturedEAN13CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeEAN8Code{
                            self.dataHandler.handleCapturedEAN8CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeCode93Code{
                            self.dataHandler.handleCapturedCode93CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeCode128Code{
                            self.dataHandler.handleCapturedCode128CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypePDF417Code{
                            self.dataHandler.handleCapturedPDF417CodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeQRCode{
                            self.dataHandler.handleCapturedQRCodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeAztecCode{
                            self.dataHandler.handleCapturedAztecCodeMetaDataObject(metadataObj);
                        }
                        else if metadataObj.type == AVMetadataObjectTypeDataMatrixCode{
                            self.dataHandler.handleCapturedDataMatrixCodeMetaDataObject(metadataObj);
                        }
                        else{
                            Utils.print("##### Other type: \(metadataObj.type) and string: \(metadataObj.stringValue)")
                        }
                        //self.stopReading()
                    }
                });
            }
        }
    }
}


