//
//  ContainerViewController.swift
//  Big Utility
//
//  Created by Devanshu Saini on 28/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import Foundation
import SlideMenuControllerSwift
class ContainerViewController: SlideMenuController {
    
    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewNames.MainStoryboard) {
            self.mainViewController = controller
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Left") {
            self.leftViewController = controller
        }
        super.awakeFromNib()
    }
    
}
