//
//  TablePicker.swift
//  BigUtility
//
//  Created by Devanshu Saini on 13/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds

@objc public protocol TablePickerDelegate: class {
    @objc optional func didCancelTablePicker()
    func didSuccessSelectionTablePicker(withSelection selection: Set<Int>, andArbitraryIndex theIndex: Int)
}

class TablePicker: BaseViewController, GADBannerViewDelegate {
    
    @IBOutlet weak var listTable: UITableView!
    
    @IBOutlet weak var advertisementView: GADBannerView!
    
    @IBOutlet weak var advertisementViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var advertisementViewHeightConstraint: NSLayoutConstraint!
    
    var allowMultipleSelection: Bool = false
    
    var listItems: Array<String>?
    
    var selectedItemIndices: Set<Int>?
    
    var isModal: Bool = false
    
    public var arbitraryIndex:Int = 0
    
    weak var delegate: TablePickerDelegate?
    
    fileprivate var doneNavButton : UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listTable.separatorStyle = .none
        self.screenName = Constants.GAScreenNames.CodeTypeScreen
        if self.selectedItemIndices == nil {
            self.selectedItemIndices = Set<Int>()
        }
        self.listTable.registerCellNib(TablePickerCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigation()
        self.loadBannerAdvertisement(inView: advertisementView, andDelegate: self)
    }
    
    fileprivate func setupNavigation() {
        let cancelNavButton : UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .plain, target: self, action: #selector(self.navCancelButtonCall))
        self.doneNavButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "Done"), style: .plain, target: self, action: #selector(self.navDoneButtonCall))
        self.doneNavButton?.isEnabled = false
        navigationItem.leftBarButtonItem = cancelNavButton
        navigationItem.rightBarButtonItem = self.doneNavButton
        navigationItem.title = Constants.ScreenNavigationTitle.CodeTypeScreen
    }
    
    func navCancelButtonCall(_ sender: AnyObject) {
        self.delegate?.didCancelTablePicker?()
        if self.isModal {
            self.dismiss(animated: true, completion: nil)
        }
        else{
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    func navDoneButtonCall(_ sender: AnyObject) {
        self.delegate?.didSuccessSelectionTablePicker(withSelection: self.selectedItemIndices!, andArbitraryIndex: self.arbitraryIndex)
        if self.isModal {
            self.dismiss(animated: true, completion: nil)
        }
        else{
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    //MARK: GADBannerViewDelegate Methods
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        self.setHiddenForAdvertisementView(forHeightConstraint: self.advertisementViewHeightConstraint, forBottomConstraint: self.advertisementViewBottomConstraint, setHidden: false, withAnimation: true)
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        Utils.print("Advertisement Error: \(error.localizedDescription)")
    }
}

extension TablePicker : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedItemIndices!.contains((indexPath as NSIndexPath).row) {
            self.selectedItemIndices!.remove((indexPath as NSIndexPath).row)
        }
        else{
            if !self.allowMultipleSelection {
                self.selectedItemIndices?.removeAll()
            }
            self.selectedItemIndices!.insert((indexPath as NSIndexPath).row)
        }
        self.doneNavButton?.isEnabled = (self.selectedItemIndices?.count)! > 0
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
    }
}

extension TablePicker : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listItems!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier : String = String.className(TablePickerCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! TablePickerCell
        let listItem: String = self.listItems![indexPath.row]
        cell.pickerLabel.text = listItem
        
        let needAccessory:Bool = self.selectedItemIndices!.contains(indexPath.row)
        cell.setCheckmark(setHidden: !needAccessory, withImageName: nil)
        
        let separatorHidden:Bool = ( indexPath.row == (self.listItems!.count - 1) )
        cell.setBottomSeparator(setHidden: separatorHidden, withColor: nil)
        return cell
    }
}


