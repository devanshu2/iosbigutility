//
//  DrawerMenuController.swift
//  Big Utility
//
//  Created by Devanshu Saini on 28/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit

class DrawerMenuController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var mainViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad();
    }
}

extension DrawerMenuController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension DrawerMenuController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier : String = "cellIdentifer"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as UITableViewCell!
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        }
        cell?.textLabel?.text = String((indexPath as NSIndexPath).row)
        return cell!
    }
}
