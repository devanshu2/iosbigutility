//
//  TablePickerCell.swift
//  BigUtility
//
//  Created by Devanshu Saini on 17/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit

class TablePickerCell: UITableViewCell {
    
    @IBOutlet weak var pickerLabel: UILabel!
    
    @IBOutlet weak var bottomSeparator: UIView!
    
    @IBOutlet weak var bottomSeparatorHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var checkMarkImageView: UIImageView!
    
    @IBOutlet weak var checkMarkWrapper: UIView!
    
    @IBOutlet weak var checkMarkWrapperWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setDefaults()
    }
    
    public func setDefaults() {
        self.pickerLabel.font = Constants.Fonts.TextFont
        self.pickerLabel.text = ""
        self.bottomSeparator.backgroundColor = UIColor.lightGray
        self.checkMarkImageView.image = UIImage(named: Constants.ImageNames.Check)
    }
    
    public func setCheckmark(setHidden hidden:Bool, withImageName imageName:String?) {
        self.checkMarkWrapperWidthConstraint.constant = hidden ? 0.0 : 30.0
        self.checkMarkWrapper.isHidden = hidden
        if imageName != nil {
            self.checkMarkImageView.image = UIImage(named: imageName!)
        }
    }
    
    public func setBottomSeparator(setHidden hidden:Bool, withColor color: UIColor?) {
        self.bottomSeparatorHeightConstraint.constant = hidden ? 0.0 : 1.0
        self.bottomSeparator.isHidden = hidden
        if color != nil {
            self.bottomSeparator.backgroundColor = color!
        }
    }
}
