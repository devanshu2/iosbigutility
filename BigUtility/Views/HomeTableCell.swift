//
//  HomeTableCell.swift
//  Big Utility
//
//  Created by Devanshu Saini on 28/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import BAFluidView

@objc public protocol HomeTableCellDelegate{
    @objc optional func gotTapOnHomeTableCell(indexPath aIndexPath:IndexPath)
}

class HomeTableCell: UITableViewCell {
    @IBOutlet weak var waveWrapper: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var circleWidthRatio: NSLayoutConstraint!
    @IBOutlet weak var theLabel: UILabel!
    @IBOutlet weak var labelBottomTrailing: NSLayoutConstraint!
    @IBOutlet weak var labelBottomView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconWrapperImageView: UIImageView!
    var indexPath: IndexPath!
    var fluidView: BAFluidView!
    weak var delegate: HomeTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fluidView = BAFluidView(frame: waveWrapper.bounds, maxAmplitude: 10, minAmplitude: 5, amplitudeIncrement: 5, startElevation: 0.5)
        fluidView.fillColor = UIColor.clear
        fluidView.strokeColor = Constants.Colors.WhiteColor
        fluidView.lineWidth = 0.0
        fluidView.keepStationary()
        fluidView.startAnimation()
        self.waveWrapper.addSubview(fluidView)
        self.circleWidthRatio.constant = 0.0
        self.labelBottomView.layer.cornerRadius = 2.5
        self.labelBottomView.clipsToBounds = true
        self.labelBottomTrailing.constant = self.theLabel.bounds.size.width
        self.labelBottomView.backgroundColor = UIColor.clear
        self.theLabel.textColor = UIColor.black
        self.textLabel?.font = Constants.Fonts.TextFont
        self.layoutIfNeeded()
    }
    
    fileprivate func hideCircle(){
        self.labelBottomTrailing.constant = self.theLabel.bounds.size.width
        self.circleWidthRatio.constant = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }) 
    }
    
    @IBAction func tapOnCellDown(_ sender: AnyObject) {
        self.labelBottomView.backgroundColor = UIColor.red
        self.labelBottomTrailing.constant = 0.0
        self.circleWidthRatio.constant = 90.0
        UIView.animate(withDuration: 0.3, animations: { 
            self.layoutIfNeeded()
        }) 
    }
    
    @IBAction func toucUpOnCell(_ sender: AnyObject) {
        self.hideCircle()
    }
    
    @IBAction func tapOnCell(_ sender: AnyObject) {
        self.hideCircle()
        self.delegate?.gotTapOnHomeTableCell!(indexPath: self.indexPath)
    }
}
