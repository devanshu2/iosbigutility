//
//  UIButton.swift
//  BigUtility
//
//  Created by Devanshu Saini on 06/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit

extension UIButton {
    func setThemeStyle() {
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.backgroundColor = Constants.Colors.ButtonColor
        self.setTitleColor(Constants.Colors.WhiteColor, for: UIControlState())
        self.setTitleColor(Constants.Colors.LightLightBlueColor, for: .highlighted)
        self.titleLabel?.font = Constants.Fonts.DefaultButtonFont
    }
}
