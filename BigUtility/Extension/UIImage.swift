//
//  UIIMage.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIImage {
    func trim(trimRect :CGRect) -> UIImage {
        if CGRect(origin: CGPoint.zero, size: self.size).contains(trimRect) {
            if let imageRef = self.cgImage?.cropping(to: trimRect) {
                return UIImage(cgImage: imageRef)
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
        self.draw(in: CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let image = trimmedImage else { return self }
        
        return image
    }
    
//    func drawWatermarkText(text: String?) -> UIImage! {
//        let textColor: UIColor = UIColor(white: 0.5, alpha: 1.0)
//        let font: UIFont = UIFont.systemFontOfSize(20.0)
//        let paddingX: Float = 20.0
//        let paddingY: Float = 20.0
//        let imageSize: CGSize = self.size
//        let attr: Dictionary = [NSForegroundColorAttributeName: textColor, NSFontAttributeName: font]
//        let textSize: CGSize = text.bridgeToObjectiveC().sizeWithAttributes(attr)
//        
//    }
//    
//    -(UIImage*)drawWatermarkText:(NSString*)text {
//    UIColor *textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
//    UIFont *font = [UIFont systemFontOfSize:50];
//    CGFloat paddingX = 20.f;
//    CGFloat paddingY = 20.f;
//    
//    // Compute rect to draw the text inside
//    CGSize imageSize = self.size;
//    NSDictionary *attr = @{NSForegroundColorAttributeName: textColor, NSFontAttributeName: font};
//    CGSize textSize = [text sizeWithAttributes:attr];
//    CGRect textRect = CGRectMake(imageSize.width - textSize.width - paddingX, imageSize.height - textSize.height - paddingY, textSize.width, textSize.height);
//    
//    // Create the image
//    UIGraphicsBeginImageContext(imageSize);
//    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
//    [text drawInRect:CGRectIntegral(textRect) withAttributes:attr];
//    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return resultImage;
//    }
}
