//
//  AppConfiguration.swift
//  BigUtility
//
//  Created by Devanshu Saini on 16/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import Foundation
import Firebase
import GoogleMobileAds

public class AppConfiguration: NSObject {
    
    static let sharedInstance : AppConfiguration = {
        let instance = AppConfiguration()
        return instance
    }()
    
    public func configureFireBase() {
        if (self.isFireBaseEventLogEnabled() || self.isAdmobEnabled()) {
            FIRApp.configure()
        }
        if self.isAdmobEnabled() {
            GADMobileAds.configure(withApplicationID: Constants.General.AdMobBigUtilityAdID)
        }
    }
    
    public func isFireBaseEventLogEnabled() -> Bool {
        return false
    }
    
    public func isAdmobEnabled() -> Bool {
        return false
    }
    
    public func isCodedImageAdvertisementWaterEnabled() -> Bool {
        return true
    }
}
