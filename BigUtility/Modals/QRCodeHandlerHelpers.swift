//
//  QRCodeHandlerHelpers.swift
//  BigUtility
//
//  Created by Devanshu Saini on 27/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import Contacts
import AddressBook

public class BarCodeBaseData: NSObject {
    
    override init() {
        super.init()
    }
    
    public func getBarcodeString() ->String {
        return ""
    }
}

class BarCodeSMSData : BarCodeBaseData {
    public var smsNumber:String!
    public var smsText:String!
    convenience init(_ smsNumber:String, _ smsText:String) {
        self.init()
        self.smsText = smsText
        self.smsNumber = smsNumber
    }
    
    convenience init?(smsDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        let splitData:Array<String>? = inputString.components(separatedBy: ":")
        if splitData != nil {
            if (splitData?.count)! > 0 {
                let identifierString:String = splitData![0]
                if identifierString == "SMSTO" {
                    if (splitData?.count)! >= 3 {
                        let smsNumber:String = splitData![1]
                        let smsText:String = splitData![2]
                        if ((smsNumber.characters.count > 0) || (smsText.characters.count > 0)) {
                            initialized = true
                            self.smsText = smsText
                            self.smsNumber = smsNumber
                        }
                    }
                }
            }
        }
        if initialized == false {
            return nil
        }
    }
    
    override func getBarcodeString() ->String {
        return "SMSTO:"+self.smsNumber+":"+self.smsText
    }
}

class BarCodeWifiData: BarCodeBaseData {
    public var SSID:String!
    public var authenticationType:String!
    public var password:String!
    public var isHidden:Bool = false
    convenience init(ssid SSID:String, authenticationType authentication:String, networkPassword password:String, andIsHidden isHidden:Bool) {
        self.init()
        self.SSID = SSID
        self.authenticationType = authentication
        self.password = password
        self.isHidden = isHidden
    }
    
    override func getBarcodeString() ->String {
        var returnString = "WIFI:T:\(self.authenticationType);S:\(self.SSID);P:\(self.password);"
        if self.isHidden {
            returnString = returnString + "H:true"
        }
        returnString = returnString + ";"
        return returnString;
    }
}

class BarCodeGeoData : BarCodeBaseData {
    public var latitude:Double!
    public var longitude:Double!
    convenience init(_ latitude:Double, _ longitude:Double) {
        self.init()
        self.longitude = longitude
        self.latitude = latitude
    }
    
    convenience init?(geoDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        let splitData:Array<String>? = inputString.components(separatedBy: ":")
        if splitData != nil {
            let identifierString:String = splitData![0]
            if identifierString == "geo" {
                if (((splitData?.count)! >= 3) && ((splitData?[1].characters.count)! > 0) && ((splitData?[2].characters.count)! > 0)) {
                    let latitude:Double = Double(splitData![1])!
                    let longitude:Double = Double(splitData![2])!
                    initialized = true
                    self.latitude = latitude
                    self.longitude = longitude
                }
            }
        }
        if initialized == false {
            return nil
        }
    }
    
    override func getBarcodeString() ->String {
        return "geo:" + String(self.latitude) + "," + String(self.longitude) + ",400"
    }
}

class BarCodeURLData: BarCodeBaseData {
    
    public var theURL:String!
    
    convenience init(inputURLString inputURL:String) {
        self.init()
        self.theURL = inputURL
    }
    
    convenience init?(urlDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        if Utils.isValidURLString(urlString: inputString) {
            self.theURL = inputString
            initialized = true
        }
        if initialized == false {
            return nil
        }
    }
    
    override func getBarcodeString() ->String {
        return self.theURL
    }
}

class BarCodeTelData: BarCodeBaseData {
    
    public var telephoneNumber:String!
    
    convenience init(inputTelePhoneString telePhoneString:String) {
        self.init()
        self.telephoneNumber = telePhoneString
    }
    
    convenience init?(telDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        let splitData:Array<String>? = inputString.components(separatedBy: ":")
        if splitData != nil {
            let identifierString:String = splitData![0]
            if identifierString == "tel" {
                if (splitData?.count)! >= 2 {
                    let phoneNumber:String = splitData![1]
                    let phoneURL = URL(string: "tel://" + phoneNumber)!
                    if UIApplication.shared.canOpenURL(phoneURL) {
                        initialized = true
                        self.telephoneNumber = phoneNumber
                    }
                }
            }
        }
        if initialized == false {
            return nil
        }
    }
    
    override func getBarcodeString() ->String {
        return "tel:" + self.telephoneNumber
    }
}

class BarCodeEmailData: BarCodeBaseData {
    
    public var emailID:String!
    
    public var subject:String!
    
    public var message:String!
    
    convenience init(emailID theEmailID:String, subject theSubject:String, message theMessage:String) {
        self.init()
        self.emailID = theEmailID
        self.subject = theSubject
        self.message = theMessage
    }
    
    convenience init?(emailDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        let splitData:Array<String>? = inputString.components(separatedBy: ":")
        if splitData != nil {
            let identifierString:String = splitData![0]
            if identifierString == "MATMSG" {
                var emailStringSplit = inputString.components(separatedBy: "MATMSG:TO:")
                if emailStringSplit.count == 2 {
                    var stringPart: String = emailStringSplit[0] as String
                    if stringPart.characters.count == 0 {
                        stringPart = emailStringSplit[1]
                        emailStringSplit = stringPart.components(separatedBy: ";SUB:")
                        if emailStringSplit.count > 1 {
                            let emailAddress: String = emailStringSplit[0]
                            emailStringSplit.remove(at: 0)
                            stringPart = emailStringSplit.joined(separator: ";SUB:")
                            emailStringSplit = stringPart.components(separatedBy: ";BODY:")
                            if emailStringSplit.count > 1 {
                                let emailSubject = emailStringSplit[0]
                                emailStringSplit.remove(at: 0)
                                stringPart = emailStringSplit.joined(separator: ";BODY:")
                                var emailBody = stringPart
                                emailStringSplit = stringPart.components(separatedBy: ";;")
                                if emailStringSplit.count > 0 {
                                    emailStringSplit.removeLast()
                                    emailBody = emailStringSplit.joined(separator: ";;")
                                }
                                initialized = true
                                self.emailID = emailAddress
                                self.subject = emailSubject
                                self.message = emailBody
                            }
                        }
                    }
                }
            }
        }
        if initialized == false {
            return nil
        }
    }
    
    override func getBarcodeString() ->String {
        return "MATMSG:TO:" + self.emailID + ";SUB:" + self.subject + ";BODY:" + self.message + ";;"
    }
}

class BarCodeVEventData: BarCodeBaseData {
    
    public var summary:String!
    
    public var dateStart:Date!
    
    public var dateEnd:Date!
    
    convenience init(eventSummary theSummary:String, dateStart theStartDate:Date, dateEnd theEndDate:Date) {
        self.init()
        self.summary = theSummary
        self.dateStart = theStartDate
        self.dateEnd = theEndDate
    }
    
    convenience init?(eventDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        var tempArray = inputString.components(separatedBy: "BEGIN:VEVENT")
        if tempArray.count > 1 {
            var summary:String?
            var dateStart:Date?
            var dateEnd:Date?
            tempArray.removeFirst()
            tempArray = tempArray.joined(separator: "BEGIN:VEVENT").components(separatedBy: "\n")
            for lineString:String in tempArray {
                if ((lineString.characters.count == 0) || (lineString == "END:VEVENT")) {
                    continue
                }
                var eventTempArray = lineString.components(separatedBy: "SUMMARY:")
                if eventTempArray.count > 1 {
                    eventTempArray.removeFirst()
                    summary = eventTempArray.joined(separator: "SUMMARY:")
                }
                
                eventTempArray = lineString.components(separatedBy: "DTSTART:")
                if eventTempArray.count > 1 {
                    eventTempArray.removeFirst()
                    dateStart = Utils.getDateFromVEventTimeStamp(theTimeStamp: eventTempArray.first!)
                }
                
                eventTempArray = lineString.components(separatedBy: "DTEND:")
                if eventTempArray.count > 1 {
                    eventTempArray.removeFirst()
                    dateEnd = Utils.getDateFromVEventTimeStamp(theTimeStamp: eventTempArray.first!)
                }
            }
            if ((summary != nil) && (dateStart != nil) && (dateEnd != nil)) {
                initialized = true
                self.summary = summary
                self.dateStart = dateStart
                self.dateEnd = dateEnd
            }
        }
        if initialized == false {
            return nil
        }
    }
    
    private func getDateString(fromDate theDate:Date) ->String {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "yyyyLLdd'T'HHmmssZ"
        let dateString = dayTimePeriodFormatter.string(from: theDate)
        return dateString
    }
    
    override func getBarcodeString() ->String {
        return "BEGIN:VEVENT\nSUMMARY:" + self.summary + "\nDTSTART:" + self.getDateString(fromDate: self.dateStart) + "\nDTEND:" + self.getDateString(fromDate: self.dateEnd) + "\nEND:VEVENT"
    }
}

class BarCodeVCardData: BarCodeBaseData {
    
    public var people:AnyObject!
    
    convenience init(contactObject contactDetails: AnyObject) {
        self.init()
        self.people = contactDetails
    }
    
    convenience init?(eventDataString inputString: String) {
        self.init()
        var initialized:Bool = false
        let tempArray = inputString.components(separatedBy: "BEGIN:VCARD")
        if tempArray.count > 1 {
            let contactData:Data = inputString.data(using: .utf8)!
            if #available(iOS 9, *) {
                var contacts = [CNContact]()
                do{
                    contacts = try CNContactVCardSerialization.contacts(with: contactData)
                } catch {
                    print("vcardFromContacts \(error)")
                }
                if !contacts.isEmpty {
                    initialized = true
                    self.people = contacts as AnyObject!
                }
            }
            else {
                let adressBook: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeUnretainedValue()
                let source: ABRecord = ABAddressBookCopyDefaultSource(adressBook).takeUnretainedValue()
                let people: [ABRecord] = ABPersonCreatePeopleInSourceWithVCardRepresentation(source, contactData as CFData!).takeUnretainedValue() as [ABRecord]
                if (people.count > 0) {
                    initialized = true
                    self.people = people as AnyObject!
                    //                let name = "\(ABRecordCopyCompositeName(people.first).takeUnretainedValue())"
                }
            }
        }
        if initialized == false {
            return nil
        }
    }
    
    override func getBarcodeString() ->String {
        var vcardstring:String = ""
        if #available(iOS 9, *) {
            do {
                let vdata:Data = try CNContactVCardSerialization.data(with: self.people as! [CNContact])
                if let outputString = String(data: vdata, encoding: .utf8) {
                    vcardstring = outputString
                }
            }
            catch {
                debugPrint(error)
            }
        }
        else {
            let vdata:Data = ABPersonCreateVCardRepresentationWithPeople(self.people as! CFArray!).takeRetainedValue() as Data
            if let outputString = String(data: vdata, encoding: .utf8) {
                vcardstring = outputString
            }
        }
        return vcardstring
    }
}
