//
//  QRCodeDataHandler.swift
//  BigUtility
//
//  Created by Devanshu Saini on 19/09/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

import UIKit
import AVFoundation
import Contacts



class BarcodeDataHandler: NSObject {
    
    public var dataType: BarCodeDataType = .String
    
    public weak var delegate: UIViewController!
    
    convenience init(classDelegate delegate: UIViewController) {
        self.init()
        self.delegate = delegate
    }
    
    public func handleCaptureData(_ capturedString: String?){
        guard ((capturedString != nil) && ((capturedString?.length)! > 0)) else {
            return
        }
        debugPrint("captured string: " + capturedString!)
        var dataToReturn:AnyObject?
        self.dataType = .String
        dataToReturn = BarCodeSMSData(smsDataString:capturedString!)
        if dataToReturn != nil {
            self.dataType = .SMS
        }
        else {
            dataToReturn = BarCodeTelData(inputTelePhoneString: capturedString!)
            if dataToReturn != nil {
                self.dataType = .Call
            }
            else {
                dataToReturn = BarCodeGeoData(geoDataString: capturedString!);
                if dataToReturn != nil {
                    self.dataType = .GeoLocation
                }
                else {
                    dataToReturn = BarCodeEmailData(emailDataString: capturedString!);
                    if dataToReturn != nil {
                        self.dataType = .Email
                    }
                    else {
                        dataToReturn = BarCodeURLData(inputURLString: capturedString!)
                        if dataToReturn != nil {
                            self.dataType = .URL
                        }
                        else {
                            dataToReturn = BarCodeVEventData(eventDataString: capturedString!);
                            if dataToReturn != nil {
                                self.dataType = .Event
                            }
                            else {
                                dataToReturn = BarCodeVCardData(eventDataString: capturedString!);
                                if dataToReturn != nil {
                                    self.dataType = .VCARD
                                }
                            }
                        }
                    }
                }
            }
        }
        Utils.print("##### the data type: " + self.dataType.rawValue)
        
        let alert = UIAlertController(title: self.dataType.rawValue, message: capturedString, preferredStyle: .alert)
        let noAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
        alert.addAction(noAction)
        self.delegate.present(alert, animated: true, completion: nil)
        
    }
    
    fileprivate func matches(for regex: String, in text: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            let arrayData:Array = results.map { nsString.substring(with: $0.range)}
            return (arrayData.count > 0)
        } catch let error as NSError {
            Utils.print("invalid regex: \(error.localizedDescription)")
            return false
        }
    }
    
    public func handleCapturedUPCECodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCaptured39CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCaptured39Mod43CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedEAN13CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedEAN8CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedCode93CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedCode128CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedPDF417CodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedQRCodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedAztecCodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
    
    public func handleCapturedDataMatrixCodeMetaDataObject(_ metaData: AVMetadataMachineReadableCodeObject){
        self.handleCaptureData(metaData.stringValue)
    }
}
